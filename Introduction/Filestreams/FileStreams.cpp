#include <fstream>
#include <iostream>
#include <string>

using namespace std;

int main()
{
   // Writing to a text file
   ofstream wfile("example.txt");
   if (wfile) {
      wfile << "This is a line\n";
      wfile << "This is a second line\n";
      wfile.close();
   }
   // Reading a file
   char line[100];
   ifstream rfile("example.txt");
   if (rfile) {
      int l = 1;
      while (rfile.getline(line, 100)) {
         cout << l << ": " << line << endl;
         l++;
      }
      rfile.close();
   } else {
      cerr << "Unable to open file" << endl;
   }

   return 0;
}

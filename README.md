# Modern C++, introduction IoT MQTT, RPi and SenseHAT in 7 steps

Several standards for C++ exist: C++03 (standard 2003), C++11 (standard 2011),
C++14 and C++17. In almost all language details these standards are backwards
compatible.

C++17 is used for compiling (g++ v7.3.0) the code examples.

Every code example can be compiled by the next Linux command:

    make

For cleaning:

    make clean
    
    make clean-depend

The make command will read the Makefile and will start compiling and linking
according to the dependencies and commands in the Makefile. 
The compiled code can be debugged because the *-g* option in the Makefiles is 
used. Some *dbg* launches are added to the Visual Studio Code file 
*launch.json*.

A limited number of code examples can still be compiled in the QtCreator IDE
(deprecated).

## Fetching updated code examples

Go to the directory where you have already downloaded an initial version of 
the repository.
Use the next git command for fetching updated code:

    git pull

## Introduction

This directory contains several basic C++ code examples necessary for
understanding the code examples in Step 1, 2, etc.

C++:

* class, object, object instantiation
* visibility, private, public
* constructor, constructor overloading, constructor initialisation list
* class member data, class member functions,setters, getters, const
* call by reference
* implementation file, header file, include guards
* output stream, cout, endl
* single line comments //

Modern C++:

* std::array template class (container class)
* copy constructor
* range based for loop
* auto (automatic deduction of the data type from its initialisation expression)
* auto: by reference (add &), by value (nothing to add), const correct (add const)

## Step 1

C++:

* namespaces (to prevent name clashes in larger programs)
* std::string, formatting, conversions (string to int), basic exception handling

## Step 2

C++:

* constructor delegation 
* output stream operator overloading
* container classes, iterators, begin(), end(), cbegin(), cend()
* lambda functions (local functions), algorithms
* this pointer, static data and functions in classes

## Step 3

C++:

* Inheritance (public, protected, private)
* IS-A relation, HAS-A relation
* Abstract base classes (ABC), virtual abstract functions = 0
* STL pairs and maps
* functors, std::function, bind()

## Step 4

C++: 

* SenseHAT and Pixel class
* Led matrix and sensor reading

## Step 5

C++:

* MQTT client class based on Mosquitto base class
* RAII (Resource Allocation Is Initialisation) 
* MQTT RAII: in ctor connect() and in dtor disconnect()
* RoombaSerialLinkDemo based on Boost C++ lib

## Step 6

C++:

* SenseHAT demo: RandomWalk, multi-threaded, shared device, mutex
* ParLoop class, std::chrono (stronger typing of time data)

## Step 7

C++:

* Typecasting
* Summary and overview

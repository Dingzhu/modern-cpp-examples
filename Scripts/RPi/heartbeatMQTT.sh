#!/bin/bash

if [ -f /etc/hostname ]; then
    hostname=$(</etc/hostname)
else
    exit 1
fi

MACaddress=$(hostname -I)
message="$hostname is up $MACaddress"
broker="broker.hivemq.com"
group="1819feb" 
topicroot="ESEiot/$group/$hostname"  

wget --spider $broker >/dev/null 2>&1
if [ "$?" == 0 ]; then
    /usr/local/bin/mosquitto_pub -h $broker -t $topicroot/up -m "$message" 
fi

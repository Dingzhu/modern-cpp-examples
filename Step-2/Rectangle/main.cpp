#include "Rectangle.h"

#include <iostream>

using namespace std;

int main()
{
   Rectangle r1;
   Rectangle r2{1.0, 3.0}; // Uniform initialization: { ..... }

   r1.setWidth(r2.getWidth() * r2.getWidth());

   cout << "r2 = " << r2 << endl;
   cout << "Surface area r2 = " << r2.surfaceArea() << endl;

   return 0;
}

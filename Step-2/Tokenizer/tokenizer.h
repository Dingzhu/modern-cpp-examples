#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <cctype>
#include <string>
#include <vector>

int isForwardSlash(int c);

// Using a default input parameter: std::isspace function pointer,
// checks wether c is a white space character.
std::vector<std::string> split(const std::string &str,
                               int delimiter(int) = std::isspace);

#endif

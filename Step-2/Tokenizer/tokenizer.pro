TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    tokenizer.cpp

HEADERS += \
    tokenizer.h

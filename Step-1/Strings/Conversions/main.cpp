// String converions and exceptions

#include <iostream>
#include <stdexcept>
#include <string>

using namespace std;

int main()
{
   //------------------------------------ string objects, uniform initialisation
   string s1{"Hello World\n"};
   string s2{"10"};
   string s3{to_string(123)};
   string s4{to_string(123.99)};

   // int stoi (const string& str, size_t* idx = 0, int base = 10);
   int ints2{stoi(s2)};

   cout << endl << s1 << " " << s2 << " = " << ints2 << endl;
   cout << s3 << " " << s4 << endl << endl;

   //---------------------------------------------------------------------------
   string s10{"123"};
   size_t pos1{0};
   int ints10{stoi(s10, &pos1)};

   cout << s10 << "==>" << ints10 << " s10 to int conversion pos1: " << pos1
        << endl;

   //---------------------------------------------------------------------------
   string s20{"123xyz"};
   size_t pos2{0};
   int ints20{stoi(s20, &pos2)};

   cout << s20 << "==>" << ints20 << " s20 to int conversion pos2: " << pos2
        << endl;
   if (s20.size() != pos2) {
      cout << "## Wrong format integer string s20: '" << s20 << "'" << endl;
   }

   //---------------------------------------------------------------------------
   try {
      string s30{"xyz123"};
      size_t pos3{0};
      int ints30{stoi(s30, &pos3)};

      cout << s30 << "==>" << ints30 << " s30 to int conversion pos3: " << pos3
           << endl;
      if (s30.size() != pos3) {
         cout << "## Wrong format integer string s30: '" << s30 << "'" << endl;
      }
   }
   catch (invalid_argument &e) {
      cout << endl << "## EXCEPTION s30: " << e.what() << endl;
   }

   cout << string(50, '-') << endl << endl;

   return 0;
}

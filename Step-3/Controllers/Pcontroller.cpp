#include "Pcontroller.h"
#include <iostream>

Pcontroller::Pcontroller(double Tsample, double P)
   : Controller{Tsample}
   , P_{P}
{
}

double Pcontroller::operator()(double err)
{
   std::cout << Controller::getCount() << std::endl;
   return P_ * err;
}

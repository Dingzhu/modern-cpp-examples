#ifndef CONTROLLER_H
#define CONTROLLER_H

/// Abstract Base Class (ABC) representing the interface for controllers.
/// Abstract classes can not be instantiated.
/// This is a functor class because the function call operator is overloaded.
/// The number of instantiated Controllers will be counted, using #count_.
class Controller
{
public:
   // Inline static function.
   // Can only use static data members.
   static int getCount() { return count_; }

   Controller(double Tsample);
   virtual ~Controller();
   // Abstract virtual function, must be defined in a derived class.
   // Function call operator: operator()
   /// Calculates the output signal every sample time #Tsample_.
   virtual double operator()(double err) = 0;

protected:
   // A static data member is shared by all objects.
   // Already available if no objects are instantiated.
   /// The number of instantiated objects.
   static int count_;
   double Tsample_;
};

#endif // CONTROLLER_H

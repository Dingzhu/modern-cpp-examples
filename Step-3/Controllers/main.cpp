#include "PIcontroller.h"
#include "Pcontroller.h"
#include <iostream>

/// Control loop sample time in seconds.
const double Ts{0.01};

/// Shows the instantion of several #Controller objects, the resulting
/// Controller object count and some output calculations.
/// @todo Implement a PIDcontroler class derived from Controller.
int main()
{
   std::cout << "#Controllers = " << Controller::getCount() << std::endl;

   Pcontroller Pcontr{Ts, 1.5};
   PIcontroller PIcontr{Ts, 1.2, 1.0};

   std::cout << "#Controllers = " << Controller::getCount() << std::endl;
   std::cout << "#Controllers = " << Pcontr.getCount() << std::endl;
   {
      PIcontroller PIcontr2{Ts, 1.2, 1.0};
      std::cout << "#Controllers = " << Controller::getCount() << std::endl;
      // PIcontr2 dtor is executed.
   }
   std::cout << "#Controllers = " << Controller::getCount() << std::endl;

   double outP = 3.3;
   double setpointP = 3.5;
   double errorP = setpointP - outP;

   // Use the object name as a function name
   outP = Pcontr(errorP);
   std::cout << "out P  controller = " << outP << std::endl;

   double outPI = 3.3;
   double setpointPI = 3.5;
   double errorPI = setpointPI - outPI;

   outPI = PIcontr(errorPI);
   std::cout << "out PI controller = " << outPI << std::endl;
   outPI = PIcontr(errorPI);
   std::cout << "out PI controller = " << outPI << std::endl;
   outPI = PIcontr(errorPI);
   std::cout << "out PI controller = " << outPI << std::endl
             << "Reset integrator " << std::endl;
   PIcontr.reset();
   outPI = PIcontr(errorPI);
   std::cout << "out PI controller = " << outPI << std::endl;

   return 0;
}

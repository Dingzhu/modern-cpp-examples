class A
{
public:
   int x;

protected:
   int y;

private:
   int z;
};

// Public inheritance
class B : public A
{
   // x is public
   // y is protected
   // z is not accessible from B
};

// Protected inheritance
class C : protected A
{
   // x is protected
   // y is protected
   // z is not accessible from C
};

// Private inheritance
class D : private A // 'private' is default for classes
{
   // x is private
   // y is private
   // z is not accessible from D
};

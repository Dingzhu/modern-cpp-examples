// C++11 STL pairs and maps

#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <utility>

using namespace std;

// Commands related functions
int start(int data);
int stop(int data);
int repeat(int data);

// Commands related class member functions
class Commands
{
public:
   int update(int data)
   {
      cout << "** exec Commands::update  ** " << data << endl;
      return data;
   }
};

// C++11 instead of typedef, using STL function template
using commandfunction = std::function<int(int)>;

int main()
{
   int i{100};
   pair<string, int> si{"one", 1};
   pair<int, int> ii{10, 100};
   pair<int, int *> ipi{i, &i};
   pair<string, commandfunction> cf{"start", start};

   cout << si.first << " --> " << si.second << endl;
   cout << ii.first << " --> " << ii.second << endl;
   cout << ipi.first << " --> " << ipi.second << endl;
   cout << cf.first << " " << cf.second(11) << endl;
   *ipi.second = 200;
   cout << "i = " << i << endl;

   Commands cmds;
   auto update = std::bind(&Commands::update, &cmds, placeholders::_1);

   // map contains sorted pairs
   map<string, commandfunction> cfs{
      {"start", start}, {"stop", stop}, {"update", update}};

   int index{1};
   for (const auto &cmd : cfs) {
      cout << index++ << ". " << cmd.first << endl;
   }
   cout << endl;
   // Calling functions by the map interface
   cfs["start"](1000);
   cfs["update"](2000);
   cfs["stop"](3000);

   // Add new function
   cfs["repeat"] = repeat;
   cfs["repeat"](2000);

   // ------------------------------------- Command processor
   string input;
   const string exitCommand("exit");
   do {
      cout << "\n\tEnter command: ";
      cin >> input;
      if (input != exitCommand) {
         auto iter_command = cfs.find(input);
         if (iter_command != end(cfs)) {
            int dataCommand{0};
            auto fnc = (*iter_command).second;
            cout << "\tInput data (int)? ";
            cin >> dataCommand;
            cout << "\tExecuting function, returned data value = "
                 << fnc(dataCommand) << endl;
         } else {
            cerr << "+++ ERROR unkown command '" << input << "'";
         }
      }
   } while (input != exitCommand);
   cout << endl;

   return 0;
}

int start(int data)
{
   cout << "** exec start ** " << data << endl;
   return data;
}

int stop(int data)
{
   cout << "** exec stop ** " << data << endl;
   return data;
}

int repeat(int data)
{
   cout << "** exec repeat ** " << data << endl;
   return data;
}

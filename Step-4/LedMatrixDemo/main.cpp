// RaspberyPi and SenseHAT application

#include "AppInfo.h"
#include "Pixel.h"
#include "SenseHAT.h"

#include <iomanip>
#include <iostream>
#include <string>

/// Shows 6 pixels doing a random walk, and the measured pressure and huminity
/// value.
int main(int argc, char *argv[])
{
   try {
      std::cout << "-- Application: " << APPNAME_VERSION << " started\n\n";

      SenseHAT sensehat;
      sensehat.leds.clear();

      std::cout << "-- Press a key ...";
      std::cin.get();
      sensehat.leds.clear(Pixel::RED);

      std::cout << "-- Press a key ...";
      std::cin.get();
      sensehat.leds.clear(Pixel::BLUE);

      std::cout << "-- Press a key ...";
      std::cin.get();
      sensehat.leds.setPixel(0, 7, Pixel(40, 80, 100));
      sensehat.leds.setPixel(0, 0, Pixel(100, 80, 40));

      std::cout << "-- Press a key ...";
      std::cin.get();

      sensehat.leds.clear(Pixel(100, 0, 0));
   }
   catch (std::exception &e) {
      std::cerr << "-- Exception " << e.what() << std::endl;
   }
   catch (...) {
      std::cerr << "-- UNKNOWN EXCEPTION\n";
   }

   std::cout << "\n-- Application: " << APPNAME_VERSION << " stopped"
             << std::endl
             << std::endl;

   return 0;
}

// C++11

#include "ClocksTimers.h"
#include <iostream>
#include <thread>

using namespace std;

int main()
{
   // time points, represent absolute time
   chrono::system_clock::time_point epoch; // default ctor
   chrono::system_clock::time_point systemStart{chrono::system_clock::now()};
   // durations
   const chrono::seconds twentySeconds{20};
   const chrono::hours aDay{24};
   const chrono::hours aYear{aDay * 365};
   const chrono::milliseconds oneMillisecond{1};
   chrono::milliseconds msecs{
      chrono::duration_cast<chrono::milliseconds>(aDay)};

   cout << endl;
   cout << "Epoch:            " << timepointAsString(epoch) << endl;
   cout << "System start:     " << timepointAsString(systemStart) << endl;

   cout << endl;
   cout << oneMillisecond.count() << " msec" << endl;
   cout << chrono::nanoseconds(oneMillisecond).count() << " nsec" << endl;
   cout << msecs.count() << " msec" << endl << endl;

   cout << "Passed time since epoch in msec = " << timeSinceEpoch_msec()
        << endl;
   cout << "Passed time since epoch in sec  = " << timeSinceEpoch_sec() << endl;

   cout << endl;
   const chrono::seconds fiveSeconds{5};
   chrono::system_clock::time_point t1{chrono::system_clock::now()};
   cout << "Sleeping ... " << timepointAsString(t1) << endl;
   // sleep_until absolute time (time point)
   this_thread::sleep_until(t1 + fiveSeconds);
   chrono::system_clock::time_point t2{chrono::system_clock::now()};
   cout << "Awake (absolute time): " << timepointAsString(t2) << endl;

   cout << endl << "Sleeping ..." << endl;
   // sleep_for duration
   this_thread::sleep_for(fiveSeconds);
   cout << "Awake (duration): " << fiveSeconds.count() << " sec" << endl
        << endl;

   return 0;
}
